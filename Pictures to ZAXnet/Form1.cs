﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Pictures_to_ZAXnet
{
    public partial class Form1 : Form
    {
        string lookInFolder= @"\\amidala\store\Prod Loads\HR Pics for ZAXnet",
            putInFolder = @"\\cliegg\websites\zaxnet\hr\photos";     //zaxnetdev
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] nwPictures = Directory.GetFiles(lookInFolder);
            foreach(string pic in nwPictures)
            {
                if(pic.ToUpper().EndsWith(".JPG"))
                {
                    try
                    {
                        File.Copy(string.Format(@"{0}", pic), string.Format(@"{0}\{1}", putInFolder, Path.GetFileName(pic)), true);
                        File.Move(string.Format(@"{0}", pic), string.Format(@"{0}\Loaded\{1}", lookInFolder, Path.GetFileName(pic)));
                    }
                    catch(Exception ex)
                    {
                        string tmp = ex.Message;
                    }
                }
            }
            Application.Exit();
        }
    }
}
